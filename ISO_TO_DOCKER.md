# Convert an ISO image to a Docker image

## WSL

> Distro: [Ubuntu 22.10](https://ubuntu.com/download/desktop) (`ubuntu-22.10-desktop-amd64.iso`)

Install `squashfs-tools` if needed:

```bash
sudo apt-get install squashfs-tools -y
```

Launch Docker Desktop on Windows.

```bash
cd ~
```

```bash
pwd
```

```bash
mv /mnt/c/Users/johnn/Downloads/ubuntu-22.10-desktop-amd64.iso ~
```

```bash
ls
```

```bash
mkdir rootfs unsquashfs
```

(run `sudo rm -rf rootfs unsquashfs` first if needed)

```bash
sudo mount -o loop ubuntu-22.10-desktop-amd64.iso rootfs
```

```bash
cd rootfs
```

```bash
find . -type f | grep filesystem.squashfs
```

Copy the first line of the input (e.g., `./casper/filesystem.squashfs`).

```bash
cd ..
```

```bash
sudo unsquashfs -f -d unsquashfs/ rootfs/casper/filesystem.squashfs
```

```bash
sudo tar -C unsquashfs -c . | docker import - ubuntu-22-10
```

```bash
docker images
```

### Create a container and run commands on it

```bash
docker run --name ubuntu-22-10-container -d -i -t ubuntu-22-10 /bin/sh
```

```bash
docker exec -it ubuntu-22-10-container echo $SHELL
```

```bash
docker exec -it ubuntu-22-10-container cat /etc/os-release
```

```bash
docker exec -it ubuntu-22-10-container file /etc/os-release
```

```bash
docker exec -it ubuntu-22-10-container file /usr/lib/os-release
```

```bash
docker exec -it ubuntu-22-10-container cat /etc/lsb-release
```

### References

- https://youtu.be/3lHGwrshnb4
- https://dev.to/sofianehamlaoui/convert-iso-images-to-docker-images-18jh

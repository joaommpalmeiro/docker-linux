# Notes

- Original name: `docker-linux`
- https://github.com/remarkjs/vscode-remark
- https://github.com/remarkjs/remark-lint
- https://github.com/remarkjs/remark/tree/main/packages/remark-cli
- https://wiki.ubuntu.com/ChangingShells
- https://github.com/remarkjs/remark-gfm
- https://github.com/remarkjs/remark/tree/main/packages/remark-stringify#api
- https://github.com/remarkjs/remark/blob/main/doc/plugins.md
- https://github.com/vscode-icons/vscode-icons vs. https://github.com/microsoft/vscode-icons + https://www.figma.com/community/file/768673354734944365
- Packages:
  - https://github.com/nexssp/os
  - https://github.com/retrohacker/getos
  - https://github.com/python-distro/distro
- https://docs.docker.com/desktop/release-notes/
- https://hub.docker.com/search?q=linux&image_filter=official
- https://www.freedesktop.org/software/systemd/man/os-release.html
- https://itsfoss.com/arm-aarch64-x86_64/:
  - `x86_64` or `x86` or `amd64`
  - `AArch64` or `arm64` or `ARMv8` or `ARMv9`
- https://docs.redhat.com/en/documentation/red_hat_virtualization/4.3/html/technical_reference/qcow2: "QCOW2 is a storage format for virtual disks. QCOW stands for QEMU copy-on-write."
- https://en.wikipedia.org/wiki/List_of_Linux_distributions
- Lima:
  - https://github.com/lima-vm/lima
  - https://lima-vm.io/docs/installation/
  - https://lima-vm.io/docs/usage/
  - https://lima-vm.io/docs/examples/
  - https://lima-vm.io/docs/templates/
  - https://lima-vm.io/docs/reference/limactl_stop/
  - https://lima-vm.io/docs/reference/limactl_prune/
  - https://lima-vm.io/docs/reference/limactl_list/
  - https://lima-vm.io/docs/reference/limactl_start/
  - https://lima-vm.io/docs/reference/limactl_create/:
    - `--arch string         machine architecture (x86_64, aarch64, riscv64)`
  - https://github.com/lima-vm/lima/tree/master/examples
  - https://github.com/lima-vm/lima/blob/f3cb0f4d0887c41dcaea81fbc5e5dc394663224d/examples/ubuntu.yaml
  - https://github.com/lima-vm/lima/blob/f6bc8addd19300f0c4c4124299ac39bed2c05b63/examples/ubuntu.yaml
  - https://github.com/lima-vm/lima/blob/f3cb0f4d0887c41dcaea81fbc5e5dc394663224d/pkg/limayaml/defaults.go#L892-L911
  - [How to create additional templates?](https://github.com/lima-vm/lima/discussions/1934) thread
  - https://github.com/lima-vm/alpine-lima
  - https://github.com/lima-vm/alpine-lima/blob/99d3e0a4f20f9100078e718f4b25d5c6ca2a5588/lima.sh
  - https://github.com/lima-vm/lima/blob/f3cb0f4d0887c41dcaea81fbc5e5dc394663224d/examples/default.yaml
  - https://lima-vm.io/docs/config/multi-arch/:
  - [Is it expected for fedora aarch64 on Intel to start in 10 minutes ?](https://github.com/lima-vm/lima/discussions/872) issue: "Yes, ARM on Intel is really slow."
- https://old-releases.ubuntu.com/releases/kinetic/
- https://old-releases.ubuntu.com/releases/kinetic/SHA256SUMS
- https://github.com/89luca89/distrobox:
  - [[Feature] MacOS host support](https://github.com/89luca89/distrobox/issues/36) issue
  - https://liv.pink/post/2023-09-12-distrobox-on-lima/
- https://github.com/NixOS/nixpkgs
- https://github.com/rancher-sandbox/rancher-desktop/
- https://askubuntu.com/questions/1042696/are-there-official-ubuntu-arm-aarch64-desktop-images
- https://alpinelinux.org/downloads/
- https://releases.ubuntu.com/
- https://releases.ubuntu.com/22.04/:
  - "Server install image": "The server install image allows you to install Ubuntu permanently on a computer for use as a server. It will not install a graphical user interface."
- https://cloud-images.ubuntu.com/:
  - https://cloud-images.ubuntu.com/releases/22.10/:
    - https://cloud-images.ubuntu.com/releases/22.10/release/
    - https://cloud-images.ubuntu.com/releases/22.10/release/SHA256SUMS
    - https://cloud-images.ubuntu.com/releases/22.10/release-20230716/
    - https://cloud-images.ubuntu.com/releases/22.10/release-20230716/SHA256SUMS
- https://containerd.io/: "An industry-standard container runtime (...)"
- https://wiki.ubuntu.com/Releases
- https://en.wikipedia.org/wiki/Ubuntu_version_history#Table_of_versions
- Debian:
  - https://github.com/lima-vm/lima/blob/f3cb0f4d0887c41dcaea81fbc5e5dc394663224d/examples/debian-12.yaml:
    - `# Try to use release-yyyyMMdd image if available. Note that release-yyyyMMdd will be removed after several months.`
  - https://gcore.com/learning/how-to-check-your-debian-version/
  - https://cloud.debian.org/images/cloud/
  - https://www.debian.org/releases/:
    - https://www.debian.org/releases/bookworm/
  - https://en.wikipedia.org/wiki/Debian_version_history

## Commands

```bash
npm install -D remark remark-gfm
```

```bash
limactl stop default && limactl delete default
```

```bash
limactl prune
```

```bash
limactl start
```

```bash
lima uname -m
```

```bash
lima cat /etc/os-release
```

```bash
limactl list
```

```bash
limactl start --list-templates
```

```bash
limactl shell debian-12 lsb_release --all
```

## Snippets

### `.remarkrc` file

```json
{
  "settings": {
    "bullet": "-",
    "listItemIndent": "one"
  },
  "plugins": ["remark-gfm"]
}
```

```markdown
- <https://github.com/remarkjs/vscode-remark> + <https://github.com/remarkjs/remark-lint> + <https://github.com/remarkjs/remark/tree/main/packages/remark-cli>
- <https://wiki.ubuntu.com/ChangingShells>
- `npm install -D remark remark-gfm`
- <https://github.com/remarkjs/remark-gfm>
- <https://github.com/remarkjs/remark/tree/main/packages/remark-stringify#api>
- <https://github.com/remarkjs/remark/blob/main/doc/plugins.md>
- <https://github.com/vscode-icons/vscode-icons> vs. <https://github.com/microsoft/vscode-icons> + <https://www.figma.com/community/file/768673354734944365>
- Packages:
  - <https://github.com/nexssp/os>
  - <https://github.com/retrohacker/getos>
  - <https://github.com/python-distro/distro>
- <https://docs.docker.com/desktop/release-notes/>
- <https://hub.docker.com/search?q=linux&image_filter=official>
- <https://www.freedesktop.org/software/systemd/man/os-release.html>
```

### `package.json` file

```json
{
  "name": "docker-linux",
  "version": "0.1.0",
  "devDependencies": {
    "remark": "14.0.2",
    "remark-gfm": "3.0.1"
  }
}
```

### `.vscode/extensions.json` file

```json
{
  "recommendations": [
    "unifiedjs.vscode-remark",
    "vscode-icons-team.vscode-icons"
  ]
}
```

### `.vscode/settings.json` file

```json
{
  "[markdown]": {
    "editor.defaultFormatter": "unifiedjs.vscode-remark",
    "editor.formatOnSave": true
  },
  "files.autoSave": "off"
}
```

### `ubuntu/README.md` file

````markdown
# Ubuntu

## [Ubuntu 22.10](https://releases.ubuntu.com/kinetic/)

```bash
mkdir -p ubuntu-22-10/etc/ && docker exec -it ubuntu-22-10-container cat /etc/os-release > ubuntu-22-10/etc/os-release
```

## Notes

- <https://releases.ubuntu.com/>
````

### Templates

```yaml
images:
  - location: "https://old-releases.ubuntu.com/releases/kinetic/ubuntu-22.10-live-server-arm64.iso"
    arch: "aarch64"
    digest: "sha256:a19d956e993a16fc6496c371e36dcc0eb85d2bdf6a8e86028b92ce62e9f585cd"
```

```yaml
containerd:
  system: false
  user: false
```

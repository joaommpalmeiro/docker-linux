# Ubuntu 22.10

Install [QEMU](https://www.qemu.org/download/) and [Lima](https://lima-vm.io/docs/installation/) (if necessary).

```bash
limactl validate templates/ubuntu-2210.yaml
```

```bash
limactl start --tty=false templates/ubuntu-2210.yaml
```

```bash
limactl shell ubuntu-2210 cat /etc/os-release
```

```bash
mkdir -p data/ubuntu-2210/etc/ && limactl shell ubuntu-2210 cat /etc/os-release > data/ubuntu-2210/etc/os-release
```

```bash
limactl stop ubuntu-2210 && limactl delete ubuntu-2210
```

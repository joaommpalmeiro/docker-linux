# Ubuntu 23.04

Install [QEMU](https://www.qemu.org/download/) and [Lima](https://lima-vm.io/docs/installation/) (if necessary).

```bash
limactl validate templates/ubuntu-2304.yaml
```

```bash
limactl start --tty=false templates/ubuntu-2304.yaml
```

```bash
limactl shell ubuntu-2304 cat /etc/os-release
```

```bash
mkdir -p data/ubuntu-2304/etc/ && limactl shell ubuntu-2304 cat /etc/os-release > data/ubuntu-2304/etc/os-release
```

```bash
limactl stop ubuntu-2304 && limactl delete ubuntu-2304
```

# Debian 12

Install [QEMU](https://www.qemu.org/download/) and [Lima](https://lima-vm.io/docs/installation/) (if necessary).

```bash
limactl validate templates/debian-12.yaml
```

```bash
limactl start --tty=false templates/debian-12.yaml
```

```bash
limactl shell debian-12 cat /etc/os-release
```

```bash
mkdir -p data/debian-12/etc/ && limactl shell debian-12 cat /etc/os-release > data/debian-12/etc/os-release
```

```bash
limactl stop debian-12 && limactl delete debian-12
```
